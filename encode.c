#include <stdio.h>
#include <stdint.h>

void encodeCharacter(char c, FILE *outputFile);
int isNotNewLine(char c);
uint8_t getParityBits(uint8_t nibble);
uint8_t getParityBit(uint8_t nibble, int index) ;
uint8_t parityBit(uint8_t nibble, int parityIndex , int exclusionPosition);
uint8_t appropriateParityBit(int sum, int parityIndex);
uint8_t append(uint8_t nibble, uint8_t parityBits);

int main(int argc, char *argv[]) {
	if (argc != 3) {
		printf("Please make sure to provide an input file and output.\n");
		return 0;
	}
	
    char *inputFileName = argv[1];
    char *outputFileName = argv[2];
    
    FILE *inputFile = fopen(inputFileName, "r");
    FILE *outputFile = fopen(outputFileName, "w");
    
    int c;
    while ((c = fgetc(inputFile)) != EOF) {
			encodeCharacter(c, outputFile);
	}
	
	printf("\n");
	printf("Reached end of file.\n");
    return fclose(inputFile) & fclose(outputFile);
}

void encodeCharacter(char c, FILE *outputFile) {
	if (isNotNewLine(c)) {
		printf("%c in hex: %x\n", c, c);
		printf("--------------\n");
		
	    uint8_t highNibble = c >> 4;
	    uint8_t parityBits = getParityBits(highNibble);
	    uint8_t encodedByte = append(highNibble, parityBits);
	    
	    printf("high nibble: %x\n", highNibble);
	    printf("parity bits: %x\n", parityBits);

		fputc(encodedByte, outputFile);
		fputc('\n', outputFile);

		printf("\n");
		
		uint8_t lowNibble = c & 0x0F;
		parityBits = getParityBits(lowNibble);
		encodedByte = append(lowNibble, parityBits);
		
		printf("low nibble: %x\n", lowNibble);
	    printf("parity bits: %x\n", parityBits);
	    
	    fputc(encodedByte, outputFile);
	    fputc('\n', outputFile);
	    
	    printf("\n");
	}
}

int isNotNewLine(char c) {
	return c != '\n';
}

uint8_t getParityBits(uint8_t nibble) {
	uint8_t result = 0x00;
	
	for (int i = 0; i < 3; i++) {
		result |= getParityBit(nibble, i);
	}
	
	return result;
}

uint8_t getParityBit(uint8_t nibble, int index) {
	switch(index) {
		case 0:
			return parityBit(nibble, index, 3);
		case 1:
			return parityBit(nibble, index, 2);
		case 2:
			return parityBit(nibble, index, 0);
	}
}

uint8_t parityBit(uint8_t nibble, int parityIndex, int exclusionPosition) {
	int sum = 0;
	uint8_t shifted = nibble;
	
	for (int i = 3; i >= 0; i--) {
		if (i != exclusionPosition) {
			sum += shifted >> i;
		}
	}
	
	return appropriateParityBit(sum, parityIndex);
}

uint8_t appropriateParityBit(int sum, int parityIndex) {
	if (sum % 2 == 0) {
		return 0x00;
	}
	
	return 0x01 << parityIndex;
}

uint8_t append(uint8_t nibble, uint8_t parityBits) {
	return (nibble << 4) | parityBits;
}
