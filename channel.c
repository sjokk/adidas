#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

static const int BYTE_SIZE = 8;

int generateRandomNumber();
void printDetails(int bitPositionArray[]);

int main(int argc, char *argv[]) {
	if (argc != 3) {
		printf("Please make sure to provide an input file and output.\n");
		return 0;
	}
	
	// Seed the random number generator for reproducible results
	// for debugging purposes.
	srand(0);
	
	// The array that will keep track of the index
	// that represents the i-th bit being flipped n times
	// bitPositionFlipped[i] = n
	int bitPositionFlipped[8] = {0};
	
	char *inputFileName = argv[1];
    char *outputFileName = argv[2];
    
    FILE *inputFile = fopen(inputFileName, "r");
    FILE *outputFile = fopen(outputFileName, "w");
    
	int c;
	int bitPosition;
	uint8_t mask;
	uint8_t byteAfterNoise;
	
    while ((c = fgetc(inputFile)) != EOF) {
		if (c != '\n') {
			bitPosition = generateRandomNumber();
			printf("Bit position %i will be flipped\n", bitPosition);
			// Create a mask by shifting a uint8_t by bitPosition to the left.
			mask = 0x01 << bitPosition;
			byteAfterNoise = c ^ mask;
			
			fputc(byteAfterNoise, outputFile);
			fputc('\n', outputFile);
			
			// Increment the number of bits in that position flipped.
			bitPositionFlipped[bitPosition]++;
		}
	}
	
	printf("\n");
	
	printDetails(bitPositionFlipped);
	
    return fclose(inputFile) & fclose(outputFile);
}

int generateRandomNumber() {
	// Return random number from 0 to BYTE_SIZE (8)
	return rand() % BYTE_SIZE;
}

void printDetails(int bitPositionArray[]) {
	int sum = 0;
	int numberOfTimesFlipped;
	
	for (int i = 0; i < BYTE_SIZE; i++) {
		numberOfTimesFlipped = bitPositionArray[i];
		sum += numberOfTimesFlipped;
		
		printf(
			"Bit position %i was flipped %i times\n", 
			i, 
			numberOfTimesFlipped
		);
	}
	
	// Sum should be equal to the number of bytes read.
	printf("\nTotal number of bits flipped: %i\n", sum);
}
